const logos = [
    {
        id: 0,
        imgId: "java",
        src: "images/java-icon-3.png",
        alt: "java-logo"
    }, 
    {
        id: 1,
        imgId: "cpp",
        src: "images/c-plus-plus-logo-2.png",
        alt: "cpp-logo"
    },
    {
        id: 2,
        imgId: "html",
        src: "images/html-logo.png",
        alt: "html-logo",
        className: "html"
    },
    {
        id: 3,
        imgId: "css",
        src: "images/css-logo.png",
        alt: "css-logo"
    },
    {
        id: 4,
        imgId: "javascript",
        src: "images/javascript-icon-5.png",
        alt: "javascript-logo"
    },
    {
        id: 5,
        imgId: "sql",
        src: "images/sql-icon-2.png",
        alt: "sql-logo"
    },
    {
        id: 6,
        imgId: "py",
        src: "images/python-icon-3.png",
        alt: "py-logo"
    },
    {
        id: 7,
        imgId: "bootstrap",
        src: "images/bootstrap-icon-full.png",
        alt: "bootstrap-logo"
    },
    {
        id: 8,
        imgId: "git",
        src: "images/git-logo-3.png",
        alt: "git-logo"
    },
    {
        id: 9,
        imgId: "github",
        src: "images/github-logo-2.png",
        alt: "github-logo"
    },
    {
        id: 10,
        imgId: "gitlab",
        src: "images/gitlab-icon-2.png",
        alt: "gitlab-logo"
}]

export default logos;