import React from "react";

function ContactMe(props) {
    const cpRightYear = (new Date).getFullYear();

    return <section class="blue-section" id="contact-me">
        <div class="bottom-section container-fluid">
            {/* <!-- <p>Contact me here; might put the link to my resume may here too</p> --> */}
            <h3>Let's chat!</h3>
            <p>If you are interested in working together, feel free to reach out!</p>
            <div class="contacts">
                <i class="ft-icons fab fa-linkedin fa-2x"></i>
                <i class="ft-icons fab fa-github fa-2x"></i>
                <i class="ft-icons fab fa-gitlab fa-2x"></i>
                <i class="ft-icons fas fa-envelope fa-2x"></i>
            </div>
            <p>© Dara Cline {cpRightYear}</p>
        </div>
    </section>
}

export default ContactMe;