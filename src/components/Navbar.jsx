import React from "react";

function Navbar(props) {
    return <nav className="navbar navbar-expand-lg containter-fluid">
    <a className="navbar-brand" href="#"></a>
    <button className="nav-button navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span className="navbar-toggler-icon">
        <i className="menu-icon fas fa-bars"></i>
        </span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
        <ul className="navbar-nav">
        <li className="nav-item active">
            <a className="nav-link" href="#title">Home <span class="sr-only">(current)</span></a>
        </li>
        <li className="nav-item">
            <a className="nav-link" href="#about-me">About Me</a>
        </li>
        <li className="nav-item">
            <a className="nav-link" href="#experience">Experience</a>
        </li>
        <li className="nav-item">
            <a className="nav-link" href="#personal-projects">Projects</a>
        </li>
        <li className="nav-item">
            <a className="nav-link" href="#contact-me">Contact Me</a>
        </li>
        </ul>
    </div>
    </nav>
}

export default Navbar;