import React from "react";

function Homepage(props) {
    return <div className="homepage-section blue-section container-fluid">
      <div className="intro">
  
        {/* <!-- <h1 class="intro-name">Hi, I'm Dara!</h1>
        <h1 class="intro-about">I am a front-end web developer based in Austin, TX. It's nice to meet you!</h1> --> */}
        <h1 className="intro-name">Hi, I'm Dara!</h1>
        <h4 className="intro-body">I am a front-end web developer based in Austin, TX. It's nice to meet you!</h4>
  
      </div>
    </div>
}

export default Homepage;