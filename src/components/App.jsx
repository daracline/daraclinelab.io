import React from "react";
// import Title from "./Title.jsx";
import Navbar from "./Navbar.jsx";
import Homepage from "./Homepage.jsx";
import AboutMe from "./AboutMe.jsx";
import Experience from "./Experience.jsx";
import PersonalProjects from "./PersonalProjects.jsx";
import ContactMe from "./ContactMe.jsx";

// function PersonalProjects(props) {
// }

function App() {
  return (
    // <div className="App">
    //   <h1>Hello World</h1>
    // </div>

    <div>
      <section className="" id="title">

        {/* <!-- Nav Bar --> */}
        <Navbar />

        {/* <!-- Home Page --> */}
        <Homepage />

      </section>

      {/* <!-- About Me --> */}
      <AboutMe />

      {/* <!-- Experience --> */}
      <Experience />

      {/* <!-- Personal Projects --> */}
      <PersonalProjects />
      
      {/* <!-- Contact Me --> */}
      <ContactMe />
    </div>
  );
}

export default App;
