import React from "react";
import projects from "./../projects.js";

function createProject(props) {
  return <div class="project-col col-xl-3 col-lg-4 col-md-6">
    {/* <!-- Project 1 --> */}
    {/* <!-- figure out hwo to make each of these cards --> */}
    <div class="card">
      <img class="card-img-top my-card-image" src={props.src} alt={props.imageAltText} />
      <div class="card-body">
        <p class="card-text">
          <a href={props.href}>{props.title}</a>
        </p>
      </div>
    </div>
  </div>
}

function PersonalProjects(props) {
    return <section class="white-section" id="personal-projects">
      <div class="projects-container container-fluid">
        <div class="projects-text">
          <p>Here are some things I've been working on.</p>
        </div>
        <div class="project-cards row">
          {projects.map(createProject)}
          
        </div>
      </div>
    </section>
}

export default PersonalProjects;