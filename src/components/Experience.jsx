import React from "react";
import logos from "./../logos.js"

function Experience(props) {

    function createSkillLogo(props) {
      return <div className="tech-logo col-lg-3 col-sm-4">
        {/* <!-- <p className="tech col-md-3 col-xs-4">java</p> --> */}
        <img className={"tech  " + (props.className ? props.className: "")} key={props.id} src={props.src} id={props.imgId} alt={props.alt} />
      </div>
    }
    
    return <section className="blue-section" id="experience">
      <div className="container-fluid">
        <p className="exp-text">Here are some languages and other technologies I've worked with:</p>

        {/* <SkillLogo src="images/java-icon-3.png" id="java" alt="java-logo"/> */}
        <div className="technologies container-fluid">
          <div className="row">
            {logos.map(createSkillLogo)}
          </div>
        </div>
  
      </div>
    </section>
}

export default Experience;