import React from "react";

function AboutMe(props) {
    return <section className="white-section" id="about-me">
      <div className="bio">
        <h3>Let me tell you a little bit about myself.</h3>
        <img className="headshot circular" src="images/Dara-Headshot.jpg" alt="Dara-headshot" />
        <div className="container-fluid">
          <p className="who-am-i">I am a web developer with a desire to tap into my creative side via front end web design and development. I hope to have a career in which I can continue to learn and grow my skills. When I'm not coding, I enjoy both partner and solo dancing, playing volleyball, video games.</p>
        </div>
      </div>
    </section>
}

export default AboutMe;