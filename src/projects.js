const projects = [
    {
        // imgId: "",
        src: "images/newsletter-signup-img.png", 
        imageAltText: "newsletter-signup-img", 
        href: "https://rocky-meadow-32728.herokuapp.com/signup",
        title: "Newsletter Signup App"
    },
    {
        // imgId: "",
        src: "images/blog-site-thumbnail.png", 
        imageAltText: "blog-site-thumbnail", 
        href: "https://still-tor-22183.herokuapp.com/",
        title: "Blog Site"
    },
    {
        // imgId: "",
        src: "images/simon-game-thumbnail.png", 
        imageAltText: "simon-game-thumbnail", 
        href: "https://daracline.gitlab.io/simongame/",
        title: "Simon Game"
    },
    {
        // imgId: "",
        src: "images/drum-kit-thumbnail.png", 
        imageAltText: "drum-kit-thumbnail", 
        href: "https://daracline.gitlab.io/drumkit",
        title: "Drum Kit"
    },
    {
        // imgId: "",
        src: "images/todo-list-thumbnail-2.png", 
        imageAltText: "todo-list-thumbnail-2", 
        href: "https://daracline.gitlab.io/todolist-react/",
        title: "To-Do List"
    },
    {
        // imgId: "",
        src: "images/final-project-screenshot.png", 
        imageAltText: "final-project-screenshot", 
        href: "https://join-our-newsletter-murex.vercel.app/?",
        title: "Join Our Newsletter"
    },
    {
        // imgId: "",
        src: "images/article-page-thumbnail.png", 
        imageAltText: "article-page-thumbnail", 
        href: "https://simple-article-listing-two.vercel.app/",
        title: "Simple Article Listing"
    },
    {
        // imgId: "",
        src: "images/testimonial-page-screenshot.png", 
        imageAltText: "testimonial-page-thumbnail", 
        href: "https://testimonial-page-weld.vercel.app/",
        title: "Testimonial Page"
    }]

export default projects;